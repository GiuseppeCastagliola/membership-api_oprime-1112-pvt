package com.odigeo.product;

import com.odigeo.product.response.BookingApiMembershipInfo;
import com.odigeo.product.v2.ProductService;
import com.odigeo.product.v2.exception.ProductException;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/product/v2")
@Consumes({APPLICATION_JSON})
@Produces({APPLICATION_JSON})
public interface MembershipProductServiceV2 extends ProductService {
    @GET
    @Path("/details/{id}")
    BookingApiMembershipInfo getBookingApiProductInfo(@NotNull @PathParam("id") String productId) throws ProductException;
}
