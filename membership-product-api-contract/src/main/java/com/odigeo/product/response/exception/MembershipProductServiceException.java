package com.odigeo.product.response.exception;

import com.odigeo.product.v2.exception.ProductException;
import com.odigeo.product.v2.model.enums.ProductType;

public class MembershipProductServiceException extends ProductException {

    private final MembershipProductServiceExceptionType membershipProductServiceExceptionType;

    public MembershipProductServiceException(MembershipProductServiceExceptionType membershipProductServiceExceptionType, String productId, ProductType productType, String message) {
        super(productId, productType, message);
        this.membershipProductServiceExceptionType = membershipProductServiceExceptionType;
    }

    public MembershipProductServiceException(MembershipProductServiceExceptionType membershipProductServiceExceptionType, String productId, ProductType productType, String message, Throwable cause) {
        super(productId, productType, message, cause);
        this.membershipProductServiceExceptionType = membershipProductServiceExceptionType;
    }

    public MembershipProductServiceExceptionType getMembershipProductServiceExceptionType() {
        return membershipProductServiceExceptionType;
    }
}
