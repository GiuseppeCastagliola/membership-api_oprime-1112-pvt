package com.odigeo.product;

import javax.ws.rs.core.Application;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ServiceApplication extends Application {

    private final Set<Object> singletons = new HashSet<Object>();

    public ServiceApplication() {
        singletons.addAll(buildRestControllers());
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }

    private List<Object> buildRestControllers() {
        List<Object> list = new ArrayList<Object>();
        list.add(new MembershipProductControllerV2());
        return list;
    }
}
