package com.odigeo.product;

import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;

public class MembershipProductServiceModuleV2 extends AbstractRestUtilsModule<MembershipProductServiceV2> {

    private final ConnectionConfiguration connectionConfiguration;

    private MembershipProductServiceModuleV2(ConnectionConfiguration connectionConfiguration, ServiceNotificator... serviceNotificators) {
        super(MembershipProductServiceV2.class, serviceNotificators);
        this.connectionConfiguration = connectionConfiguration;
    }

    @Override
    protected ServiceConfiguration<MembershipProductServiceV2> getServiceConfiguration(Class<MembershipProductServiceV2> serviceContractClass) {
        final ServiceConfiguration.Builder<MembershipProductServiceV2> builder = new ServiceConfiguration.Builder<MembershipProductServiceV2>(serviceContractClass).
                withConnectionConfiguration(connectionConfiguration);
        return builder.build();
    }

    @SuppressWarnings({"PMD.AccessorClassGeneration"})
    public static class Builder extends AbstractBuilder<Builder> {

        public static void foo() {
            // Present to avoid PMD message...
        }

        public MembershipProductServiceModuleV2 build(ServiceNotificator... serviceNotificators) {
            ConnectionConfiguration connectionConfiguration = new ConnectionConfiguration();
            connectionConfiguration.setConnectionTimeoutInMillis(getConnectionTimeoutInMillis());
            connectionConfiguration.setMaxConcurrentConnections(getMaxConcurrentConnections());
            connectionConfiguration.setSocketTimeoutInMillis(getSocketTimeoutInMillis());
            connectionConfiguration.setCredentials(getCredentials());
            return new MembershipProductServiceModuleV2(connectionConfiguration, serviceNotificators);
        }
        
    }
}
