package com.odigeo.membership.client.enhanced;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.MembershipService;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.request.auth.MembershipModuleCredentials;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MembershipServiceModuleTest extends ModuleTest {

    public static final String TEST_N = "testN";
    public static final String TEST_P = "testP";
    public static final String SITE = "site";
    public static final int CONNECTION_TIMEOUT_IN_MILLIS = 60000;
    public static final int MAX_CONCURRENT_CONNECTIONS = 100;
    private MembershipModuleCredentials credentials;
    private MembershipServiceModule membershipServiceModule;

    @BeforeMethod
    public void initConfigurationEngine() {
        credentials = new MembershipModuleCredentials(TEST_N, TEST_P);
        membershipServiceModule = new MembershipServiceModule.Builder()
                .withConnectionTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
                .withSocketTimeoutInMillis(CONNECTION_TIMEOUT_IN_MILLIS)
                .withMaxConcurrentConnections(MAX_CONCURRENT_CONNECTIONS)
                .withCredentials(new MembershipModuleCredentials(TEST_N, TEST_P))
                .build(new DoNothingServiceNotificator());
        ConfigurationEngine.init(membershipServiceModule);
    }

    @Test
    public void testModuleConfigurationAndAvailability() throws InvalidParametersException {
        MembershipService membershipService = ConfigurationEngine.getInstance(MembershipService.class);
        Assert.assertTrue(membershipService.isMembershipPerksActiveOn(SITE));
        Assert.assertEquals(membershipServiceModule
                .getServiceConfiguration(MembershipService.class)
                .getConnectionConfiguration().getCredentials(), credentials);
    }
}
