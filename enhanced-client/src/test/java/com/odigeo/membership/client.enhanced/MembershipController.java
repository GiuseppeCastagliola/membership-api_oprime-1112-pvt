package com.odigeo.membership.client.enhanced;

import com.odigeo.membership.MembershipService;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.exception.MembershipNotFoundException;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.request.CheckMemberOnPassengerListRequest;
import com.odigeo.membership.request.pricing.SearchCriteriaRequest;
import com.odigeo.membership.request.product.AddToBlackListRequest;
import com.odigeo.membership.request.product.CreateMembershipSubscriptionRequest;
import com.odigeo.membership.request.product.UpdateMemberAccountRequest;
import com.odigeo.membership.request.product.UpdateMembershipRequest;
import com.odigeo.membership.request.product.creation.CreateMembershipRequest;
import com.odigeo.membership.response.BlackListedPaymentMethod;
import com.odigeo.membership.response.Membership;
import com.odigeo.membership.response.MembershipRenewal;
import com.odigeo.membership.response.MembershipStatus;
import com.odigeo.membership.response.MembershipSubscriptionOffer;
import com.odigeo.membership.response.MembershipSubscriptionOfferDetails;
import com.odigeo.membership.response.WebsiteMembership;

import javax.validation.constraints.NotNull;
import java.util.List;

public class MembershipController implements MembershipService {

    private static final UnsupportedOperationException UNSUPPORTED_OPERATION_EXCEPTION = new UnsupportedOperationException("Not needed. This is just a test Stub");

    @Override
    public Membership getMembership(Long userId, String site) throws InvalidParametersException, MembershipServiceException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;
    }

    @Override
    public WebsiteMembership getAllMembership(Long userId) throws MembershipServiceException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;
    }

    @Override
    public Boolean applyMembership(CheckMemberOnPassengerListRequest checkMemberOnPassengerListRequest) throws InvalidParametersException, MembershipServiceException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;
    }

    @Override
    public MembershipStatus disableMembership(Long memberId) throws InvalidParametersException, MembershipServiceException, MembershipNotFoundException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;
    }

    @Override
    public MembershipStatus reactivateMembership(Long userId) throws InvalidParametersException, MembershipServiceException, MembershipNotFoundException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;
    }

    @Override
    public MembershipStatus expireMembership(Long memberId) throws InvalidParametersException, MembershipServiceException, MembershipNotFoundException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;
    }

    @Override
    public MembershipRenewal disableAutoRenewal(Long memberId) throws InvalidParametersException, MembershipServiceException, MembershipNotFoundException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;
    }

    @Override
    public MembershipRenewal enableAutoRenewal(Long memberId) throws InvalidParametersException, MembershipServiceException, MembershipNotFoundException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;
    }

    @Override
    public Boolean isMembershipPerksActiveOn(String siteId) throws InvalidParametersException, MembershipServiceException {
        return true;
    }

    @Override
    public MembershipSubscriptionOffer getMembershipSubscriptionOffer(SearchCriteriaRequest searchCriteriaRequest) throws InvalidParametersException, MembershipServiceException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;
    }

    @Override
    public MembershipSubscriptionOfferDetails getMembershipSubscriptionOfferDetails(String offerId) throws InvalidParametersException, MembershipServiceException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;
    }

    @Override
    public Long createMembershipSubscription(CreateMembershipSubscriptionRequest memberAccountRequest) throws MembershipServiceException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;
    }

    @Override
    public Long createMembership(CreateMembershipRequest createMembershipRequest) throws MembershipServiceException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;
    }

    @Override
    public Boolean updateMemberAccount(UpdateMemberAccountRequest memberAccountRequest) throws InvalidParametersException, MembershipServiceException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;

    }

    @Override
    public Boolean updateMembership(UpdateMembershipRequest membershipRequest) throws InvalidParametersException, MembershipServiceException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;

    }

    @Override
    public Boolean retryMembershipActivation(Long bookingId) throws InvalidParametersException, MembershipServiceException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;
    }

    @Override
    public List<BlackListedPaymentMethod> getBlacklistedPaymentMethods(Long membershipId) throws InvalidParametersException, MembershipServiceException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;
    }

    @Override
    public Boolean addToBlackList(Long membershipId, AddToBlackListRequest addToBlackListRequest) throws InvalidParametersException, MembershipServiceException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;
    }

    @Override
    public String getNewVatModelDate() throws MembershipServiceException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;
    }

    @Override
    public Boolean isMembershipToBeRenewed(@NotNull Long membershipId) throws MembershipServiceException {
        throw UNSUPPORTED_OPERATION_EXCEPTION;
    }

}
