package com.odigeo.membership.client.enhanced;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class ModuleTest {
    public static final String BUILDER_WEB_XML = "builder-web.xml";
    private WebAppServer webAppServer;

    @BeforeClass
    public void setUp() throws Exception {
        webAppServer = new WebAppServer(BUILDER_WEB_XML);
        webAppServer.startServer();
    }

    @AfterClass
    public void setDown() throws Exception {
        webAppServer.stopServer();
    }
}
