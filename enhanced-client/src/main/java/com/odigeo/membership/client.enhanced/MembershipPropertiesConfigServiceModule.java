package com.odigeo.membership.client.enhanced;

import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.membership.MembershipPropertiesConfigService;
import com.odigeo.membership.request.auth.MembershipModuleCredentials;

public class MembershipPropertiesConfigServiceModule extends AbstractRestUtilsModule<MembershipPropertiesConfigService> {

    private static final int DEFAULT_CONNECTION_TIMEOUT_IN_MILLIS = 60 * 1000;
    private static final int DEFAULT_SOCKET_TIMEOUT_IN_MILLIS = 60 * 1000;
    private static final int DEFAUTL_POOL_SIZE = 50;

    private final ConnectionConfiguration connectionConfiguration;

    private MembershipPropertiesConfigServiceModule(ConnectionConfiguration connectionConfiguration, ServiceNotificator... serviceNotificators) {
        super(MembershipPropertiesConfigService.class, serviceNotificators);
        this.connectionConfiguration = connectionConfiguration;
    }

    @Override
    protected ServiceConfiguration<MembershipPropertiesConfigService> getServiceConfiguration(final Class<MembershipPropertiesConfigService> serviceContractClass) {
        final ServiceConfiguration.Builder<MembershipPropertiesConfigService> builder = new ServiceConfiguration.Builder<>(serviceContractClass).
                withConnectionConfiguration(connectionConfiguration);
        return builder.build();
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static class Builder {
        private int connectionTimeoutInMillis;
        private int socketTimeoutInMillis;
        private int maxConcurrentConnections;
        private MembershipModuleCredentials credentials;

        public Builder() {
            connectionTimeoutInMillis = DEFAULT_CONNECTION_TIMEOUT_IN_MILLIS;
            socketTimeoutInMillis = DEFAULT_SOCKET_TIMEOUT_IN_MILLIS;
            maxConcurrentConnections = DEFAUTL_POOL_SIZE;
        }

        public MembershipPropertiesConfigServiceModule.Builder withConnectionTimeoutInMillis(int connectionTimeoutInMillis) {
            this.connectionTimeoutInMillis = connectionTimeoutInMillis;
            return this;
        }

        public MembershipPropertiesConfigServiceModule.Builder withSocketTimeoutInMillis(int socketTimeoutInMillis) {
            this.socketTimeoutInMillis = socketTimeoutInMillis;
            return this;
        }

        public MembershipPropertiesConfigServiceModule.Builder withMaxConcurrentConnections(int maxConcurrentConnections) {
            this.maxConcurrentConnections = maxConcurrentConnections;
            return this;
        }

        public MembershipPropertiesConfigServiceModule.Builder withCredentials(MembershipModuleCredentials credentials) {
            this.credentials = credentials;
            return this;
        }

        public MembershipPropertiesConfigServiceModule build(ServiceNotificator... serviceNotificators) {
            ConnectionConfiguration connectionConfiguration = new ConnectionConfiguration.Builder()
                    .connectionTimeoutInMillis(connectionTimeoutInMillis)
                    .maxConcurrentConnections(maxConcurrentConnections)
                    .socketTimeoutInMillis(socketTimeoutInMillis)
                    .credentials(credentials)
                    .build();
            return new MembershipPropertiesConfigServiceModule(connectionConfiguration, serviceNotificators);
        }

    }
}
