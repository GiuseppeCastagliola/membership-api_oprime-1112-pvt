package com.odigeo.membership.request.search;

import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import com.odigeo.utils.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import static java.lang.Boolean.TRUE;
import static org.testng.Assert.assertEquals;

public class MemberAccountSearchRequestTest extends BeanTest<MemberAccountSearchRequest> {

    private static final String NAME = "name";
    private static final String LAST_NAME = "lastName";
    private static final Long USER_ID = 123L;
    private static final String JSON_QUERYSTRING = "{\"firstName\":\"" + NAME + "\",\"lastName\":\"" + LAST_NAME + "\",\"userId\":\"" + USER_ID + "\", \"withMemberships\":\"true\"}";


    @Override
    protected MemberAccountSearchRequest getBean() {
        return new MemberAccountSearchRequest.Builder()
                .firstName(NAME)
                .lastName(LAST_NAME)
                .userId(USER_ID)
                .withMemberships(TRUE)
                .build();
    }

    @Override
    protected boolean checkEquals() {
        return true;
    }

    @Override
    protected boolean checkHashCode() {
        return true;
    }

    @Test
    public void testJsonSearchQuery() {
        MemberAccountSearchRequest memberAccountSearchRequest = MemberAccountSearchRequest.valueOf(JSON_QUERYSTRING);
        assertEquals(memberAccountSearchRequest, getBean());
        assertEquals(MemberAccountSearchRequest.valueOf(memberAccountSearchRequest.toString()), memberAccountSearchRequest);
    }

    @Test(expectedExceptions = MembershipInternalServerErrorException.class)
    public void testWrongJsonSearchQuery() {
        String malformedJson = JSON_QUERYSTRING.replace(":", ";");
        MemberAccountSearchRequest.valueOf(malformedJson);
    }

    @Test
    public void testEquals() {
        EqualsVerifier.forClass(MemberAccountSearchRequest.class)
                .suppress(Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}
