package com.odigeo.membership.request.product;

import com.odigeo.utils.BeanTest;

public class UpdateMemberAccountRequestTest extends BeanTest<UpdateMemberAccountRequest> {

    @Override
    protected UpdateMemberAccountRequest getBean() {
        return new UpdateMemberAccountRequest();
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }
}
