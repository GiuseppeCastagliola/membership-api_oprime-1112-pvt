package com.odigeo.membership.request.search;

import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import com.odigeo.utils.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static java.lang.Boolean.TRUE;
import static org.testng.Assert.assertEquals;

public class MembershipSearchRequestTest extends BeanTest<MembershipSearchRequest> {

    private static final String ENABLED = "ENABLED";
    private static final String EUR = "EUR";
    private static final String DATE = LocalDate.now().format(DateTimeFormatter.ISO_DATE);
    private static final String BASIC = "BASIC";
    private static final String PRODUCT_STATUS = "CONTRACT";
    private static final String SOURCE_TYPE = "FUNNEL_BOOKING";
    private static final String ACTIVATED = "ACTIVATED";
    private static final String WEBSITE = "ES";

    @Override
    protected MembershipSearchRequest getBean() {
        return new MembershipSearchRequest.Builder()
                .autoRenewal(ENABLED)
                .currencyCode(EUR)
                .fromActivationDate(DATE)
                .toActivationDate(DATE)
                .fromExpirationDate(DATE)
                .toExpirationDate(DATE)
                .fromCreationDate(DATE)
                .toCreationDate(DATE)
                .minBalance(BigDecimal.ONE)
                .maxBalance(BigDecimal.TEN)
                .memberAccountSearchRequest(new MemberAccountSearchRequest.Builder().build())
                .membershipType(BASIC)
                .monthsDuration(3)
                .productStatus(PRODUCT_STATUS)
                .sourceType(SOURCE_TYPE)
                .status(ACTIVATED)
                .totalPrice(BigDecimal.TEN)
                .website(WEBSITE)
                .withMemberAccount(TRUE)
                .withStatusActions(TRUE)
                .build();
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return true;
    }

    @Test
    public void testJsonSearchQuery() {
        assertEquals(MembershipSearchRequest.valueOf(getBean().toString()), getBean());
    }

    @Test(expectedExceptions = MembershipInternalServerErrorException.class)
    public void testWrongJsonSearchQuery() {
        String wrongJson = getBean().toString().replace(":", "-");
        MembershipSearchRequest.valueOf(wrongJson);
    }

    @Test
    public void testEquals() {
        EqualsVerifier.forClass(MembershipSearchRequest.class)
                .suppress(Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}
