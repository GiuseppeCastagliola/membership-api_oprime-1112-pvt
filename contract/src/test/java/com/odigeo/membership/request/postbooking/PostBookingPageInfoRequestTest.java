package com.odigeo.membership.request.postbooking;

import com.odigeo.utils.BeanTest;

public class PostBookingPageInfoRequestTest extends BeanTest<PostBookingPageInfoRequest> {

    private static final String TOKEN = "token";

    @Override
    protected PostBookingPageInfoRequest getBean() {
        return new PostBookingPageInfoRequest().setToken(TOKEN);
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }
}
