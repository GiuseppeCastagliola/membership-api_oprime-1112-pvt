package com.odigeo.membership.request.product;

import com.odigeo.utils.BeanTest;

public class UpdateMembershipRequestTest extends BeanTest<UpdateMembershipRequest> {

    private static final long USER_CREDIT_CARD_ID = 1234L;

    @Override
    protected UpdateMembershipRequest getBean() {
        return new UpdateMembershipRequest().setUserCreditCardId(USER_CREDIT_CARD_ID);
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }
}
