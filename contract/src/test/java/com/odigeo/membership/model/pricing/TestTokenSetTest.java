package com.odigeo.membership.model.pricing;


import com.odigeo.utils.BeanTest;

import java.util.ArrayList;
import java.util.List;

public class TestTokenSetTest extends BeanTest<TestTokenSet> {

    @Override
    protected TestTokenSet getBean() {
        return new TestTokenSet();
    }

    @Override
    protected List<TestTokenSet> getBeans() {
        TestTokenSet bean = getBean();
        List<TestTokenSet> beans = new ArrayList<TestTokenSet>();
        beans.add(getBean());
        beans.add(new TestTokenSet(
                bean.getDimensionPartitionNumberMap()
        ));
        return beans;
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }
}
