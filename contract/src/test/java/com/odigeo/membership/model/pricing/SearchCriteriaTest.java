package com.odigeo.membership.model.pricing;

import com.odigeo.utils.BeanTest;

import java.util.ArrayList;
import java.util.List;

public class SearchCriteriaTest extends BeanTest<SearchCriteria> {

    @Override
    protected SearchCriteria getBean() {
        SearchCriteria bean = new SearchCriteria();
        bean.setWebsiteCode("WebsiteCode");
        bean.setDynpackSearch(true);
        bean.setAnInterface("Interface");
        bean.setAdultsNumber(1);
        bean.setChildrenNumber(2);
        bean.setInfantsNumber(3);
        bean.setCheckInDate("");
        bean.setCheckOutDate("");
        bean.setFirstItineraryDate("");
        bean.setLastItineraryDate("");
        bean.setHasAccommodation(Boolean.TRUE);
        bean.setHasItinerary(Boolean.TRUE);
        bean.setMarketingPortal("MKP");
        bean.setTestTokenSet(new TestTokenSet());
        bean.setQaMode(true);
        bean.setVisitId((long) 4444);
        bean.setMembershipId((long) 4444);
        return bean;
    }

    @Override
    protected List<SearchCriteria> getBeans() {
        SearchCriteria bean = getBean();
        List<SearchCriteria> beans = new ArrayList<SearchCriteria>();
        beans.add(getBean());
        SearchCriteria searchCriteriaDescriptor = new SearchCriteria(
                bean.getWebsiteCode(),
                bean.getDynpackSearch(),
                bean.getAnInterface(),
                bean.getAdultsNumber(),
                bean.getChildrenNumber(),
                bean.getInfantsNumber(),
                bean.getCheckInDate(),
                bean.getCheckOutDate(),
                bean.getFirstItineraryDate(),
                bean.getLastItineraryDate(),
                bean.getTestTokenSet(),
                bean.getQaMode(),
                bean.getHasAccommodation(),
                bean.getHasAccommodation(),
                bean.getMarketingPortal(),
                bean.getVisitId(),
                bean.getMembershipId()
        );
        searchCriteriaDescriptor.setVisitId(bean.getVisitId());
        beans.add(searchCriteriaDescriptor);
        return beans;
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }
}
