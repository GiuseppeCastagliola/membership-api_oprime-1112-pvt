package com.odigeo.membership.response;

import com.odigeo.utils.BeanTest;

public class WebsiteMembershipTest extends BeanTest<WebsiteMembership> {

    @Override
    protected WebsiteMembership getBean() {
        return new WebsiteMembership();
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }
}
