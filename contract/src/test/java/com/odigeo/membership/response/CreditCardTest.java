package com.odigeo.membership.response;

import com.odigeo.utils.BeanTest;

public class CreditCardTest extends BeanTest<CreditCard> {

    @Override
    protected CreditCard getBean() {
        CreditCard creditCard = new CreditCard();
        creditCard.setId(1L);
        return creditCard;
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }
}
