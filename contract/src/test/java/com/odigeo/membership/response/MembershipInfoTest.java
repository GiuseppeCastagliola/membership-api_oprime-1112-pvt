package com.odigeo.membership.response;

import com.odigeo.utils.BeanTest;

import java.math.BigDecimal;

public class MembershipInfoTest extends BeanTest<MembershipInfo> {

    private static final int MONTHS_DURATION_12 = 12;

    @Override
    protected MembershipInfo getBean() {
        MembershipInfo membershipInfo = new MembershipInfo();
        membershipInfo.setMonthsDuration(MONTHS_DURATION_12);
        membershipInfo.setBalance(BigDecimal.ZERO);
        membershipInfo.setTotalPrice(BigDecimal.ZERO);
        return membershipInfo;
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }
}
