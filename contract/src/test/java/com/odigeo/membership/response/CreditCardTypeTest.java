package com.odigeo.membership.response;

import com.odigeo.utils.BeanTest;

public class CreditCardTypeTest extends BeanTest<CreditCardType> {

    @Override
    protected CreditCardType getBean() {
        return new CreditCardType();
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }
}
