package com.odigeo.membership.response;

import com.odigeo.utils.BeanTest;

import java.math.BigDecimal;

public class MembershipSubscriptionOfferDetailsTest extends BeanTest<MembershipSubscriptionOfferDetails> {

    private static final long PRODUCT_ID = 1234L;
    private static final String ES = "ES";
    private static final BigDecimal PRICE = BigDecimal.ZERO;
    private static final String EVALUATION_ID = "0001";
    private static final String EUR = "EUR";
    private static final String MEMBERSHIP_ID = "0002";

    @Override
    protected MembershipSubscriptionOfferDetails getBean() {
        return new MembershipSubscriptionOfferDetails(PRODUCT_ID, ES, PRICE, EVALUATION_ID, EUR, MEMBERSHIP_ID);
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }
}
