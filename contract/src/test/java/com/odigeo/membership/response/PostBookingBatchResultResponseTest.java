package com.odigeo.membership.response;


import com.odigeo.utils.BeanTest;

public class PostBookingBatchResultResponseTest extends BeanTest<PostBookingBatchResultResponse> {

    @Override
    protected PostBookingBatchResultResponse getBean() {
        return new PostBookingBatchResultResponse();
    }

    @Override
    protected boolean checkEquals() {
        return false;
    }

    @Override
    protected boolean checkHashCode() {
        return false;
    }
}
