package com.odigeo.membership;

import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.exception.MembershipNotFoundException;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.request.CheckMemberOnPassengerListRequest;
import com.odigeo.membership.request.pricing.SearchCriteriaRequest;
import com.odigeo.membership.request.product.AddToBlackListRequest;
import com.odigeo.membership.request.product.CreateMembershipSubscriptionRequest;
import com.odigeo.membership.request.product.UpdateMemberAccountRequest;
import com.odigeo.membership.request.product.UpdateMembershipRequest;
import com.odigeo.membership.request.product.creation.CreateMembershipRequest;
import com.odigeo.membership.response.BlackListedPaymentMethod;
import com.odigeo.membership.response.Membership;
import com.odigeo.membership.response.MembershipRenewal;
import com.odigeo.membership.response.MembershipStatus;
import com.odigeo.membership.response.MembershipSubscriptionOffer;
import com.odigeo.membership.response.MembershipSubscriptionOfferDetails;
import com.odigeo.membership.response.WebsiteMembership;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jboss.resteasy.spi.validation.ValidateRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.util.List;

import static com.odigeo.membership.utils.Constants.JSON_MIME_TYPE;

@Path("/membership/v1")
@ValidateRequest
@Api(value = "Membership operations", tags = "Membership v1")
public interface MembershipService {

    @GET
    @Path("/{userId}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Find membership by userId and website. Provide website in the end of the URL like this: \";website={websiteCode}\"",
            notes = "Sample request: http://serverDomain/membership/membership/v1/686868;website=OPFR")
    Membership getMembership(
            @NotNull @PathParam("userId") Long userId,
            @NotNull @MatrixParam("website") String site) throws InvalidParametersException, MembershipServiceException;

    @GET
    @Path("/{userId}/all")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Find all active memberships by userId",
            notes = "Sample request: http://serverDomain/membership/membership/v1/686868/all")
    WebsiteMembership getAllMembership(@NotNull @PathParam("userId") Long userId) throws InvalidParametersException, MembershipServiceException;

    @POST
    @Path("/applicable")
    @Consumes({JSON_MIME_TYPE})
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Check if membership discount can be applied for the user with given id and name",
            notes = "Sample request: http://serverDomain/membership/membership/v1/applicable "
                    + "Request body: {\"userId\": 686868, \"site\": \"OPFR\", \"travellerContainerList\": [{\"name\": \"John\", \"lastNames\": \"Smith\"}]}")
    Boolean applyMembership(@Valid CheckMemberOnPassengerListRequest checkMemberOnPassengerListRequest) throws InvalidParametersException, MembershipServiceException;

    @PUT
    @Path("/disable/{id}")
    @Produces({JSON_MIME_TYPE})
    @Deprecated
    @ApiOperation(value = "Change membership status to DEACTIVATED if it currently is ACTIVATED",
            notes = "Sample request: http://serverDomain/membership/membership/v1/disable/1085")
    MembershipStatus disableMembership(@NotNull @PathParam("id") Long membershipId) throws InvalidParametersException, MembershipServiceException, MembershipNotFoundException;

    @PUT
    @Path("/reactivate/{id}")
    @Produces({JSON_MIME_TYPE})
    @Deprecated
    @ApiOperation(value = "Change membership status to ACTIVATED if it is currently DEACTIVATED",
            notes = "Sample request: http://serverDomain/membership/membership/v1/reactivate/2901")
    MembershipStatus reactivateMembership(@NotNull @PathParam("id") Long membershipId) throws InvalidParametersException, MembershipServiceException, MembershipNotFoundException;

    @PUT
    @Path("/expire/{id}")
    @Produces({JSON_MIME_TYPE})
    @Deprecated
    @ApiOperation(value = "Change membership status to EXPIRED",
            notes = "Sample request: http://serverDomain/membership/membership/v1/expire/1020")
    MembershipStatus expireMembership(@NotNull @PathParam("id") Long membershipId) throws InvalidParametersException, MembershipServiceException, MembershipNotFoundException;

    @PUT
    @Path("/disable-auto-renewal/{id}")
    @Produces({JSON_MIME_TYPE})
    @Deprecated
    @ApiOperation(value = "Change auto renewal flag of the membership to DISABLED if it currently is ENABLED",
            notes = "Sample request: http://serverDomain/membership/membership/v1/disable-auto-renewal/1020")
    MembershipRenewal disableAutoRenewal(@NotNull @PathParam("id") Long membershipId) throws InvalidParametersException, MembershipServiceException, MembershipNotFoundException;

    @PUT
    @Path("/enable-auto-renewal/{id}")
    @Produces({JSON_MIME_TYPE})
    @Deprecated
    @ApiOperation(value = "Change auto renewal flag of the membership to ENABLED if it currently is DISABLED",
            notes = "Sample request: http://serverDomain/membership/membership/v1/enable-auto-renewal/1021")
    MembershipRenewal enableAutoRenewal(@NotNull @PathParam("id") Long membershipId) throws InvalidParametersException, MembershipServiceException, MembershipNotFoundException;

    @GET
    @Path("/website/{siteId}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Check if memberships are supported on given site with given interface",
            notes = "Sample request: http://serverDomain/membership/membership/v1/website/OPDE")
    Boolean isMembershipPerksActiveOn(@NotNull @PathParam("siteId") String siteId) throws InvalidParametersException;

    @POST
    @Path("/product")
    @Consumes({JSON_MIME_TYPE})
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Get membership subscription offer",
            notes = "Provide dates in format yyyy-MM-dd, for example, 2019-06-22")
    MembershipSubscriptionOffer getMembershipSubscriptionOffer(@Valid SearchCriteriaRequest searchCriteriaRequest) throws InvalidParametersException, MembershipServiceException;

    @GET
    @Path("/offer/{offerId}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Get details of membership subscription offer by offerId",
            notes = "Sample request: http://serverDomain/membership/membership/v1/offer/31611")
    MembershipSubscriptionOfferDetails getMembershipSubscriptionOfferDetails(@NotNull @PathParam("offerId") String offerId) throws InvalidParametersException, MembershipServiceException;

    @POST
    @Path("/createMembership")
    @Consumes({JSON_MIME_TYPE})
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Create membership",
            notes = "Sample request: http://serverDomain/membership/membership/v1/createMembership"
                    + "Sample body: {\"userId\": \"7\",\"name\": \"John\",\"lastNames\": \"Smith\",\"website\": \"IT\", \"mothsToRenewal\": \"12\"}")
    Long createMembershipSubscription(@Valid CreateMembershipSubscriptionRequest membershipSubscriptionRequest) throws MembershipServiceException;

    @POST
    @Path("/memberships")
    @Consumes({JSON_MIME_TYPE})
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Creates a membership",
            notes = "Sample request: http://serverDomain/membership/membership/v1/memberships"
                    + "Sample body (new subscription): {\"typeInfo\": \"CreatePendingToCollectRequest\", \"website\": \"IT\", \"mothsToRenewal\": \"12\", \"sourceType\": \"FUNNEL_BOOKING\", "
                    + "\"membershipType\": \"BUSINESS\", \"userId\": \"7\",\"name\": \"John\",\"lastNames\": \"Smith\"}"
                    + "Sample body (Pending to collect): {\"typeInfo\": \"CreatePendingToCollectRequest\", \"website\": \"ES\", \"monthsToRenewal\": 6, \"sourceType\": \"FUNNEL_BOOKING\", "
                    + "\"membershipType\": \"BUSINESS\", \"memberAccountId\": \"763\", \"expirationDate\": \"2019-02-05\", \"subscriptionPrice\": 34.99, \"currencyCode\": \"EUR\", \"recurringId\": \"abc\"}")
    Long createMembership(@Valid CreateMembershipRequest createMembershipRequest) throws MembershipServiceException;

    @PUT
    @Path("/updateMemberAccount")
    @Consumes({JSON_MIME_TYPE})
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Update memberAccount by memberAccountId. Operation is needed to define operation (without it will fall to default: UPDATE_NAMES)\n"
            + "Operations available: UPDATE_NAMES, UPDATE_USER_ID")
    Boolean updateMemberAccount(@Valid UpdateMemberAccountRequest memberAccountRequest) throws InvalidParametersException, MembershipServiceException;

    @PUT
    @Path("/updateMembership")
    @Consumes({JSON_MIME_TYPE})
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Update membership by membershipId and operation needed, available operations: REACTIVATE_MEMBERSHIP, DEACTIVATE_MEMBERSHIP,"
            + " EXPIRE_MEMBERSHIP, ENABLE_AUTO_RENEWAL, DISABLE_AUTO_RENEWAL, CONSUME_MEMBERSHIP_REMNANT_BALANCE ",
            notes = "This endpoint should be used in replacement of the deprecated ones (still working)")
    Boolean updateMembership(@Valid UpdateMembershipRequest membershipRequest) throws InvalidParametersException, MembershipServiceException;

    @PUT
    @Path("/retry-membership-activation/{bookingId}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Retry activating membership of given bookingId")
    Boolean retryMembershipActivation(@NotNull @PathParam("bookingId") Long bookingId) throws InvalidParametersException, MembershipServiceException;

    @GET
    @Path("/paymentMethods/blacklist/{id}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Return any blacklisted payment method for the given membership, if any. An empty list if none found.",
            notes = "Sample request: http://serverDomain/membership/membership/v1/paymentMethods/blacklist/686868")
    List<BlackListedPaymentMethod> getBlacklistedPaymentMethods(@NotNull @PathParam("id") Long membershipId) throws InvalidParametersException, MembershipServiceException;

    @POST
    @Path("/paymentMethods/blacklist/{id}")
    @Consumes({JSON_MIME_TYPE})
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Blacklist the given payment method associated with the membership.",
            notes = "Sample request: {\"blackListedPaymentMethods\": [{\"id\": 3,\"timestamp\": 1554390401666,\"errorType\": \"errorType\",\"errorMessage\": \"errorMessage\"}]}")
    Boolean addToBlackList(@NotNull @PathParam("id") Long membershipId, @Valid AddToBlackListRequest addToBlackListRequest) throws InvalidParametersException, MembershipServiceException;

    @GET
    @Path("/newVatModelDate")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Returns the cutover date for the new VAT model")
    String getNewVatModelDate() throws MembershipServiceException;

    @GET
    @Path("/can-renew/{membershipId}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Checks if membership is active and expiration date has passed")
    Boolean isMembershipToBeRenewed(@NotNull @PathParam("membershipId") Long membershipId) throws MembershipServiceException;

}
