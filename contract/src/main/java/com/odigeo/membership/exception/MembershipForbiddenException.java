package com.odigeo.membership.exception;

import javax.ws.rs.core.Response;

public class MembershipForbiddenException extends MembershipServiceException {

    public MembershipForbiddenException(String message) {
        this(message, null);
    }

    public MembershipForbiddenException(String message, Throwable cause) {
        super(Response.Status.FORBIDDEN, message, cause);
    }

}
