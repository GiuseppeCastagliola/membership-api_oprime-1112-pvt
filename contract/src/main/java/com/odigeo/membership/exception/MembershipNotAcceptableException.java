package com.odigeo.membership.exception;


import javax.ws.rs.core.Response;

public class MembershipNotAcceptableException extends MembershipServiceException {

    public MembershipNotAcceptableException(String message) {
        this(message, null);
    }

    public MembershipNotAcceptableException(String message, Throwable cause) {
        super(Response.Status.NOT_ACCEPTABLE, message, cause);
    }

}
