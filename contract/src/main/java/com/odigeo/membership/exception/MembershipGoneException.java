package com.odigeo.membership.exception;

import javax.ws.rs.core.Response;

public class MembershipGoneException extends MembershipServiceException {

    public MembershipGoneException(String message) {
        this(message, null);
    }

    public MembershipGoneException(String message, Throwable cause) {
        super(Response.Status.GONE, message, cause);
    }

}
