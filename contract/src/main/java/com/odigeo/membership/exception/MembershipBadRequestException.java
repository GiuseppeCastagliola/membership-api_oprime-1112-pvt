package com.odigeo.membership.exception;


import javax.ws.rs.core.Response;

public class MembershipBadRequestException extends MembershipServiceException {

    public MembershipBadRequestException(String message) {
        this(message, null);
    }

    public MembershipBadRequestException(String message, Throwable cause) {
        super(Response.Status.BAD_REQUEST, message, cause);
    }

}
