package com.odigeo.membership.exception;


import javax.ws.rs.core.Response;

public class MembershipNoContentException extends MembershipServiceException {

    public MembershipNoContentException(String message) {
        this(message, null);
    }

    public MembershipNoContentException(String message, Throwable cause) {
        super(Response.Status.NO_CONTENT, message, cause);
    }

}
