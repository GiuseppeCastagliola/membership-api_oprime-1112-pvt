package com.odigeo.membership.exception;

import javax.ws.rs.core.Response;

/**
 * Created by roc.arajol on 29-Jan-18.
 */
public class InvalidParametersException extends MembershipServiceException {

    /**
     * @deprecated Should not be used.
     *     This exception should probably have been removed previously
     *     but clients may have coded for it.
     *
     *     Use MembershipBadRequestException or MembershipForbiddenException instead
     */
    @Deprecated
    public InvalidParametersException(InvalidParametersReason invalidParametersReason, String message, Throwable cause) {
        super(invalidParametersReason.getResponseStatus(), message, cause);
    }

    /**
     * @deprecated Should not be used.
     *     This exception should probably have been removed previously
     *     but clients may have coded for it.
     *
     *     Use MembershipBadRequestException or MembershipForbiddenException instead
     */
    @Deprecated
    public InvalidParametersException(InvalidParametersReason invalidParametersReason, String message) {
        this(invalidParametersReason, message, null);
    }

    public enum InvalidParametersReason {
        BAD_REQUEST(Response.Status.BAD_REQUEST),
        FORBIDDEN(Response.Status.FORBIDDEN);

        private Response.Status status;

        InvalidParametersReason(Response.Status status) {
            this.status = status;
        }

        Response.Status getResponseStatus() {
            return status;
        }
    }
}
