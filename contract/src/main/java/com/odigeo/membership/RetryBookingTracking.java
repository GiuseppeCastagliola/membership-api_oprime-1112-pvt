package com.odigeo.membership;

import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.exception.MembershipServiceException;
import org.jboss.resteasy.spi.validation.ValidateRequest;

import javax.validation.constraints.NotNull;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import static com.odigeo.membership.utils.Constants.JSON_MIME_TYPE;

@Path("/tracking/v1")
@ValidateRequest
public interface RetryBookingTracking {

    @PUT
    @Path("/retry-booking-tracking/{bookingId}")
    @Produces({JSON_MIME_TYPE})
    Boolean retryBookingTracking(@NotNull @PathParam("bookingId") Long bookingId) throws InvalidParametersException, MembershipServiceException;

}
