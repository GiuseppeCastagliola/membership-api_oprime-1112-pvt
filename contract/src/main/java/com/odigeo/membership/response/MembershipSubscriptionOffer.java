package com.odigeo.membership.response;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MembershipSubscriptionOffer implements Serializable {
    private Long productId;
    private String website;
    private String offerId;
    private BigDecimal price;
    private String currencyCode;
    private String membershipId;

    public MembershipSubscriptionOffer() {
    }

    public MembershipSubscriptionOffer(Long productId, String website, BigDecimal price, String currencyCode, String membershipId)  {
        this.productId = productId;
        this.website = website;
        this.price = price;
        this.currencyCode = currencyCode;
        this.membershipId = membershipId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public MembershipSubscriptionOffer setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    public String getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(String membershipId) {
        this.membershipId = membershipId;
    }
}
