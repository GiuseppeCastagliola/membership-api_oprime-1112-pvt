package com.odigeo.membership.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MemberStatusAction implements Serializable {

    private Long id;
    private Long memberId;
    private String action;
    private Date timestamp;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Date getTimestamp() {
        if (timestamp != null) {
            return new Date(timestamp.getTime());
        }
        return null;
    }

    public void setTimestamp(Date timestamp) {
        if (timestamp != null) {
            this.timestamp = new Date(timestamp.getTime());
        }
    }
}
