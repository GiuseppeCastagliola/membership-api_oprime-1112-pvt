package com.odigeo.membership.response.search;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MemberAccountResponse implements Serializable {
    private long id;
    private long userId;
    private String name;
    private String lastNames;
    private List<MembershipResponse> memberships;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastNames() {
        return lastNames;
    }

    public void setLastNames(String lastNames) {
        this.lastNames = lastNames;
    }

    public List<MembershipResponse> getMemberships() {
        return memberships;
    }

    public void setMemberships(List<MembershipResponse> memberships) {
        this.memberships = memberships;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MemberAccountResponse that = (MemberAccountResponse) o;
        return id == that.id && userId == that.userId
                && Objects.equals(name, that.name) && Objects.equals(lastNames, that.lastNames)
                && Objects.equals(memberships, that.memberships);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, name, lastNames, memberships);
    }
}
