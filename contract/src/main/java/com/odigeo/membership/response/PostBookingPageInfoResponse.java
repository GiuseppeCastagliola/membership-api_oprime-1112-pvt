package com.odigeo.membership.response;

import java.util.Objects;

public class PostBookingPageInfoResponse {
    private long bookingId;
    private String email;

    public long getBookingId() {
        return bookingId;
    }

    public PostBookingPageInfoResponse setBookingId(long bookingId) {
        this.bookingId = bookingId;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public PostBookingPageInfoResponse setEmail(String email) {
        this.email = email;
        return this;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        PostBookingPageInfoResponse that = (PostBookingPageInfoResponse) obj;
        return Objects.equals(email, that.email)
                && Objects.equals(bookingId, that.bookingId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookingId, email);
    }
}
