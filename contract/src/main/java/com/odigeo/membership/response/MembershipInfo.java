package com.odigeo.membership.response;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MembershipInfo extends MembershipBaseResponse implements Serializable {

    private String lastStatusModificationDate;
    private long bookingIdSubscription;
    private String timestamp;
    private String currencyCode;
    private BigDecimal totalPrice;

    public MembershipInfo() {
    }

    public MembershipInfo(Membership membership) {
        this.setName(membership.getName());
        this.setLastNames(membership.getLastNames());
        this.setMemberId(membership.getMemberId());
        this.setMembershipId(membership.getMembershipId());
        this.setMemberAccountId(membership.getMemberAccountId());
        this.setWebsite(membership.getWebsite());
        this.setWarnings(membership.getWarnings());
        this.setAutoRenewalStatus(membership.getAutoRenewalStatus());
        this.setMembershipStatus(membership.getMembershipStatus());
        this.setActivationDate(membership.getActivationDate());
        this.setExpirationDate(membership.getExpirationDate());
        this.setRecurringId(membership.getRecurringId());
        this.setMembershipType(membership.getMembershipType());
        this.setBalance(membership.getBalance());
        this.setSourceType(membership.getSourceType());
        this.setMonthsDuration(membership.getMonthsDuration());
    }

    public String getLastStatusModificationDate() {
        return lastStatusModificationDate;
    }

    public void setLastStatusModificationDate(final String lastStatusModificationDate) {
        this.lastStatusModificationDate = lastStatusModificationDate;
    }

    public long getBookingIdSubscription() {
        return bookingIdSubscription;
    }

    public void setBookingIdSubscription(long bookingIdSubscription) {
        this.bookingIdSubscription = bookingIdSubscription;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final String timestamp) {
        this.timestamp = timestamp;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(final String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(final BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }
}
