package com.odigeo.membership.response;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MembershipSubscriptionOfferDetails extends MembershipSubscriptionOffer {
    private String evaluationId;

    public MembershipSubscriptionOfferDetails() {
    }

    public MembershipSubscriptionOfferDetails(Long productId, String website, BigDecimal price, String evaluationId, String currencyCode, String membershipId)  {
        super(productId, website, price, currencyCode, membershipId);
        this.evaluationId = evaluationId;
    }

    public String getEvaluationId() {
        return evaluationId;
    }

    public void setEvaluationId(String evaluationId) {
        this.evaluationId = evaluationId;
    }
}
