package com.odigeo.membership;

import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.request.search.MemberAccountSearchRequest;
import com.odigeo.membership.request.search.MembershipSearchRequest;
import com.odigeo.membership.response.search.MemberAccountResponse;
import com.odigeo.membership.response.search.MembershipResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jboss.resteasy.spi.validation.ValidateRequest;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import java.util.List;

import static com.odigeo.membership.utils.Constants.JSON_MIME_TYPE;

@Path("/search/v1")
@ValidateRequest
@Api(value = "Search membership resources", tags = "Membership search v1")
public interface MembershipSearchApi {

    @GET
    @Path("/memberAccounts/{id}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Find memberAccount by id. The response could include the memberships related to the account if the query parameter withMemberships it's set to true",
            notes = "Sample request: http://serverDomain/membership/search/v1/memberAccounts/123456?withMemberships=true")
    MemberAccountResponse getMemberAccount(@PathParam("id") Long memberAccountId, @DefaultValue("false") @QueryParam("withMemberships") boolean withMemberships) throws MembershipServiceException;

    @GET
    @Path("/memberAccounts")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Find memberAccounts by search parameters. The response could include the memberships, if the query parameter withMemberships is set to true,"
            + " the search parameter should be provided in json, and possible values to perform the search are the fields of the MemberAccountSearchRequest object (userId, firstName, lastName, withMemberships)"
            + "At least one search parameter should be provided",
            notes = "Sample request: http://serverDomain/membership/search/v1/memberAccounts?search={\"firstName\":\"Mario\",\"lastName\":\"Gomez\",\"userId\":\"123L\", \"withMemberships\":\"true\"}")
    List<MemberAccountResponse> searchAccounts(@QueryParam("search") MemberAccountSearchRequest searchRequest) throws MembershipServiceException;

    @GET
    @Path("/memberships/{id}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Find membership by id.", notes = "Sample request: http://serverDomain/membership/search/v1/memberships/123456")
    MembershipResponse getMembership(@PathParam("id") Long membershipId) throws MembershipServiceException;

    @GET
    @Path("/memberships")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Find memberships by search parameters. The response could include the memberStatusActions, if the query parameter withStatusActions it's set to true,"
            + " and MemberAccount related to the memberships if withMemberAccount it's set to true. "
            + "The search parameter should be provided in json, and possible values to perform the search are the fields of the MembershipSearchRequest object (...) note that it's possible to filter also for the values of the MemberAccount search "
            + "At least one search parameter should be provided",
            notes = "Sample request: http://serverDomain/membership/search/v1/memberships?search={\"website\":\"OPFR\", \"withStatusActions\":\"true\", \"withMemberAccount\":\"true\",\"memberAccountSearchRequest\":{\"userId\":\"12533\"}}")
    List<MembershipResponse> searchMemberships(@QueryParam("search") MembershipSearchRequest searchRequest) throws MembershipServiceException;
}
