package com.odigeo.membership;

import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.exception.MembershipServiceException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jboss.resteasy.spi.validation.ValidateRequest;

import javax.validation.constraints.NotNull;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import static com.odigeo.membership.utils.Constants.JSON_MIME_TYPE;

@Path("/back-office/v1")
@ValidateRequest
@Api(description = "Membership Back Office Operations", tags = "Membership back office v1")
public interface MembershipBackOfficeService {

    @PUT
    @Path("/update-membership-marketing-info/{membershipId}")
    @Produces({JSON_MIME_TYPE})
    @ApiOperation(value = "Sends the information of the membership to marketing",
        notes = "Sample request: http://serverDomain/membership/back-office/v1/update-membership-marketing-info/154;email=test@edreams.com")
    Boolean updateMembershipMarketingInfo(@NotNull @PathParam("membershipId") Long membershipId,
        @NotNull @MatrixParam("email") String email) throws InvalidParametersException, MembershipServiceException;
}
