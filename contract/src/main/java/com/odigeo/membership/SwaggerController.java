package com.odigeo.membership;

import io.swagger.jaxrs.listing.ApiListingResource;

import javax.ws.rs.Path;

@Path("/api-docs/swagger.{type:json|yaml}")
public class SwaggerController extends ApiListingResource {

}
