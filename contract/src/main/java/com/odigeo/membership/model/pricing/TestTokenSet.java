package com.odigeo.membership.model.pricing;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.Map;


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class TestTokenSet {
    private Map<String, Integer> dimensionPartitionNumberMap;

    public TestTokenSet() {
    }

    public TestTokenSet(Map<String, Integer> dimensionPartitionNumberMap) {
        this.dimensionPartitionNumberMap = dimensionPartitionNumberMap;
    }

    public Map<String, Integer> getDimensionPartitionNumberMap() {
        return dimensionPartitionNumberMap;
    }
}
