package com.odigeo.membership.model.pricing;

import io.swagger.annotations.ApiParam;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class SearchCriteria {

    @NotNull
    private String websiteCode;
    @NotNull
    private Boolean hasAccommodation;
    @NotNull
    private Boolean hasItinerary;
    @NotNull
    private Boolean dynpackSearch;
    @NotNull
    private String anInterface;
    @Min(0)
    private int adultsNumber;
    @Min(0)
    private int childrenNumber;
    @Min(0)
    private int infantsNumber;

    @ApiParam(format = "yyyy-MM-dd", example = "2019-06-22")
    private String checkInDate;
    private String checkOutDate;
    private String firstItineraryDate;
    private String lastItineraryDate;

    private String marketingPortal;
    @NotNull
    private TestTokenSet testTokenSet;

    private Boolean qaMode;
    private Long visitId;
    private Long membershipId;

    public SearchCriteria() {
    }

    @SuppressWarnings("PMD.ExcessiveParameterList")
    public SearchCriteria(String websiteCode,
                          Boolean dynpackSearch, String anInterface, int adultsNumber, int childrenNumber, int infantsNumber,
                          String checkInDate, String checkOutDate, String firstItineraryDate, String lastItineraryDate,
                          TestTokenSet testTokenSet, Boolean qaMode, Boolean hasAccommodation, Boolean hasItinerary,
                          String marketingPortal, Long visitId, Long membershipId) {
        this.websiteCode = websiteCode;
        this.dynpackSearch = dynpackSearch;
        this.anInterface = anInterface;
        this.adultsNumber = adultsNumber;
        this.childrenNumber = childrenNumber;
        this.infantsNumber = infantsNumber;
        this.checkInDate = checkInDate;
        this.checkOutDate = checkOutDate;
        this.firstItineraryDate = firstItineraryDate;
        this.lastItineraryDate = lastItineraryDate;
        this.testTokenSet = testTokenSet;
        this.qaMode = qaMode;
        this.hasAccommodation = hasAccommodation;
        this.hasItinerary = hasItinerary;
        this.marketingPortal = marketingPortal;
        this.visitId = visitId;
        this.membershipId = membershipId;
    }

    public String getWebsiteCode() {
        return websiteCode;
    }

    public void setWebsiteCode(String websiteCode) {
        this.websiteCode = websiteCode;
    }

    public Boolean getDynpackSearch() {
        return dynpackSearch;
    }

    public void setDynpackSearch(Boolean dynpackSearch) {
        this.dynpackSearch = dynpackSearch;
    }

    public int getAdultsNumber() {
        return adultsNumber;
    }

    public void setAdultsNumber(int adultsNumber) {
        this.adultsNumber = adultsNumber;
    }

    public int getChildrenNumber() {
        return childrenNumber;
    }

    public void setChildrenNumber(int childrenNumber) {
        this.childrenNumber = childrenNumber;
    }

    public int getInfantsNumber() {
        return infantsNumber;
    }

    public void setInfantsNumber(int infantsNumber) {
        this.infantsNumber = infantsNumber;
    }

    public TestTokenSet getTestTokenSet() {
        return testTokenSet;
    }

    public void setTestTokenSet(TestTokenSet testTokenSet) {
        this.testTokenSet = testTokenSet;
    }

    public Boolean getQaMode() {
        return qaMode;
    }

    public void setQaMode(Boolean qaMode) {
        this.qaMode = qaMode;
    }

    public Long getVisitId() {
        return visitId;
    }

    public void setVisitId(Long visitId) {
        this.visitId = visitId;
    }

    public Boolean getHasAccommodation() {
        return hasAccommodation;
    }

    public void setHasAccommodation(Boolean hasAccommodation) {
        this.hasAccommodation = hasAccommodation;
    }

    public Boolean getHasItinerary() {
        return hasItinerary;
    }

    public void setHasItinerary(Boolean hasItinerary) {
        this.hasItinerary = hasItinerary;
    }

    public String getAnInterface() {
        return anInterface;
    }

    public void setAnInterface(String anInterface) {
        this.anInterface = anInterface;
    }

    public String getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(String checkInDate) {
        this.checkInDate = checkInDate;
    }

    public String getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(String checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public String getFirstItineraryDate() {
        return firstItineraryDate;
    }

    public void setFirstItineraryDate(String firstItineraryDate) {
        this.firstItineraryDate = firstItineraryDate;
    }

    public String getLastItineraryDate() {
        return lastItineraryDate;
    }

    public void setLastItineraryDate(String lastItineraryDate) {
        this.lastItineraryDate = lastItineraryDate;
    }

    public String getMarketingPortal() {
        return marketingPortal;
    }

    public void setMarketingPortal(String marketingPortal) {
        this.marketingPortal = marketingPortal;
    }

    public Long getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(Long membershipId) {
        this.membershipId = membershipId;
    }
}
