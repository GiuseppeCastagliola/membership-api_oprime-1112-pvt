package com.odigeo.membership.request.search;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Objects;

@SuppressWarnings("PMD.GodClass")
@JsonIgnoreProperties(ignoreUnknown = true)
public class MembershipSearchRequest extends SearchRequest {

    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipSearchRequest.class);
    private final String website;
    private final String status;
    private final String autoRenewal;
    private final String fromExpirationDate;
    private final String toExpirationDate;
    private final String fromActivationDate;
    private final String toActivationDate;
    private final String fromCreationDate;
    private final String toCreationDate;
    private final String membershipType;
    private final BigDecimal minBalance;
    private final BigDecimal maxBalance;
    private final Integer monthsDuration;
    private final String productStatus;
    private final BigDecimal totalPrice;
    private final String currencyCode;
    private final String sourceType;
    private final Long memberAccountId;
    private final boolean withStatusActions;
    private final boolean withMemberAccount;
    private final MemberAccountSearchRequest memberAccountSearchRequest;

    public String getWebsite() {
        return website;
    }

    public String getStatus() {
        return status;
    }

    public String getAutoRenewal() {
        return autoRenewal;
    }

    public String getFromExpirationDate() {
        return fromExpirationDate;
    }

    public String getFromActivationDate() {
        return fromActivationDate;
    }

    public String getToExpirationDate() {
        return toExpirationDate;
    }

    public String getToActivationDate() {
        return toActivationDate;
    }

    public String getFromCreationDate() {
        return fromCreationDate;
    }

    public String getToCreationDate() {
        return toCreationDate;
    }

    public String getMembershipType() {
        return membershipType;
    }

    public BigDecimal getMinBalance() {
        return minBalance;
    }

    public BigDecimal getMaxBalance() {
        return maxBalance;
    }

    public Integer getMonthsDuration() {
        return monthsDuration;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getSourceType() {
        return sourceType;
    }

    public Long getMemberAccountId() {
        return memberAccountId;
    }

    public boolean isWithStatusActions() {
        return withStatusActions;
    }

    public MemberAccountSearchRequest getMemberAccountSearchRequest() {
        return memberAccountSearchRequest;
    }

    public boolean isWithMemberAccount() {
        return withMemberAccount;
    }

    public static Builder builder() {
        return new Builder();
    }

    @JsonIgnore
    public MembershipSearchRequest(Builder builder) {
        website = builder.website;
        status = builder.status;
        autoRenewal = builder.autoRenewal;
        fromExpirationDate = builder.fromExpirationDate;
        fromActivationDate = builder.fromActivationDate;
        toExpirationDate = builder.toExpirationDate;
        toActivationDate = builder.toActivationDate;
        fromCreationDate = builder.fromCreationDate;
        toCreationDate = builder.toCreationDate;
        membershipType = builder.membershipType;
        minBalance = builder.minBalance;
        maxBalance = builder.maxBalance;
        monthsDuration = builder.monthsDuration;
        productStatus = builder.productStatus;
        totalPrice = builder.totalPrice;
        currencyCode = builder.currencyCode;
        sourceType = builder.sourceType;
        withStatusActions = builder.withStatusActions;
        withMemberAccount = builder.withMemberAccount;
        memberAccountSearchRequest = builder.memberAccountSearchRequest;
        memberAccountId = builder.memberAccountId;
    }

    @SuppressWarnings("PMD.ExcessiveParameterList")
    @JsonCreator
    private MembershipSearchRequest(@JsonProperty("website") String website,
                                    @JsonProperty("status") String status,
                                    @JsonProperty("autoRenewal") String autoRenewal,
                                    @JsonProperty("fromExpirationDate") String fromExpirationDate,
                                    @JsonProperty("toExpirationDate") String toExpirationDate,
                                    @JsonProperty("fromActivationDate") String fromActivationDate,
                                    @JsonProperty("toActivationDate") String toActivationDate,
                                    @JsonProperty("fromCreationDate") String fromCreationDate,
                                    @JsonProperty("toCreationDate") String toCreationDate,
                                    @JsonProperty("membershipType") String membershipType,
                                    @JsonProperty("minBalance") BigDecimal minBalance,
                                    @JsonProperty("maxBalance") BigDecimal maxBalance,
                                    @JsonProperty("monthsDuration") Integer monthsDuration,
                                    @JsonProperty("productStatus") String productStatus,
                                    @JsonProperty("totalPrice") BigDecimal totalPrice,
                                    @JsonProperty("currencyCode") String currencyCode,
                                    @JsonProperty("sourceType") String sourceType,
                                    @JsonProperty("memberAccountId") Long memberAccountId,
                                    @JsonProperty("withStatusAction") boolean withStatusAction,
                                    @JsonProperty("withMemberAccount") boolean withMemberAccount,
                                    @JsonProperty("memberAccountSearch") MemberAccountSearchRequest memberAccountSearchRequest) {
        this.website = website;
        this.status = status;
        this.autoRenewal = autoRenewal;
        this.fromExpirationDate = fromExpirationDate;
        this.fromActivationDate = fromActivationDate;
        this.toExpirationDate = toExpirationDate;
        this.toActivationDate = toActivationDate;
        this.fromCreationDate = fromCreationDate;
        this.toCreationDate = toCreationDate;
        this.membershipType = membershipType;
        this.minBalance = minBalance;
        this.maxBalance = maxBalance;
        this.monthsDuration = monthsDuration;
        this.productStatus = productStatus;
        this.totalPrice = totalPrice;
        this.currencyCode = currencyCode;
        this.sourceType = sourceType;
        this.withStatusActions = withStatusAction;
        this.withMemberAccount = withMemberAccount;
        this.memberAccountSearchRequest = memberAccountSearchRequest;
        this.memberAccountId = memberAccountId;
    }

    public static class Builder {
        private String website;
        private String status;
        private String autoRenewal;
        private String fromExpirationDate;
        private String toExpirationDate;
        private String fromActivationDate;
        private String toActivationDate;
        private String fromCreationDate;
        private String toCreationDate;
        private String membershipType;
        private BigDecimal minBalance;
        private BigDecimal maxBalance;
        private Integer monthsDuration;
        private String productStatus;
        private BigDecimal totalPrice;
        private String currencyCode;
        private String sourceType;
        private Long memberAccountId;
        private boolean withStatusActions;
        private boolean withMemberAccount;
        private MemberAccountSearchRequest memberAccountSearchRequest;

        public Builder website(String website) {
            this.website = website;
            return this;
        }

        public Builder status(String status) {
            this.status = status;
            return this;
        }

        public Builder autoRenewal(String autoRenewal) {
            this.autoRenewal = autoRenewal;
            return this;
        }

        public Builder fromExpirationDate(String fromExpirationDate) {
            this.fromExpirationDate = fromExpirationDate;
            return this;
        }

        public Builder fromActivationDate(String fromActivationDate) {
            this.fromActivationDate = fromActivationDate;
            return this;
        }

        public Builder toExpirationDate(String toExpirationDate) {
            this.toExpirationDate = toExpirationDate;
            return this;
        }

        public Builder toActivationDate(String toActivationDate) {
            this.toActivationDate = toActivationDate;
            return this;
        }

        public Builder fromCreationDate(String fromCreationDate) {
            this.fromCreationDate = fromCreationDate;
            return this;
        }

        public Builder toCreationDate(String toCreationDate) {
            this.toCreationDate = toCreationDate;
            return this;
        }

        public Builder membershipType(String membershipType) {
            this.membershipType = membershipType;
            return this;
        }

        public Builder minBalance(BigDecimal minBalance) {
            this.minBalance = minBalance;
            return this;
        }

        public Builder maxBalance(BigDecimal maxBalance) {
            this.maxBalance = maxBalance;
            return this;
        }

        public Builder monthsDuration(Integer monthsDuration) {
            this.monthsDuration = monthsDuration;
            return this;
        }

        public Builder productStatus(String productStatus) {
            this.productStatus = productStatus;
            return this;
        }

        public Builder totalPrice(BigDecimal totalPrice) {
            this.totalPrice = totalPrice;
            return this;
        }

        public Builder currencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public Builder sourceType(String sourceType) {
            this.sourceType = sourceType;
            return this;
        }

        public Builder memberAccountId(Long memberAccountId) {
            this.memberAccountId = memberAccountId;
            return this;
        }

        public Builder withStatusActions(boolean withStatusActions) {
            this.withStatusActions = withStatusActions;
            return this;
        }

        public Builder withMemberAccount(boolean withMemberAccount) {
            this.withMemberAccount = withMemberAccount;
            return this;
        }

        public Builder memberAccountSearchRequest(MemberAccountSearchRequest memberAccountSearchRequest) {
            this.memberAccountSearchRequest = memberAccountSearchRequest;
            return this;
        }

        public MembershipSearchRequest build() {
            return new MembershipSearchRequest(this);
        }
    }

    public static MembershipSearchRequest valueOf(String jsonQueryParam) {
        try {
            return OBJECT_MAPPER.readValue(jsonQueryParam, MembershipSearchRequest.class);
        } catch (IOException e) {
            LOGGER.error("Invalid jsonQueryParameter : {}", jsonQueryParam, e);
            throw new MembershipInternalServerErrorException(e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public boolean equals(Object membershipSearchRequest) {
        if (this == membershipSearchRequest) {
            return true;
        }
        if (membershipSearchRequest == null || getClass() != membershipSearchRequest.getClass()) {
            return false;
        }
        MembershipSearchRequest that = (MembershipSearchRequest) membershipSearchRequest;
        return (withStatusActions == that.withStatusActions) && (withMemberAccount == that.withMemberAccount)
                && Objects.equals(website, that.website) && Objects.equals(status, that.status)
                && Objects.equals(autoRenewal, that.autoRenewal) && Objects.equals(fromExpirationDate, that.fromExpirationDate)
                && Objects.equals(toExpirationDate, that.toExpirationDate) && Objects.equals(fromActivationDate, that.fromActivationDate)
                && Objects.equals(toCreationDate, that.toCreationDate) && Objects.equals(fromCreationDate, that.fromCreationDate)
                && Objects.equals(toActivationDate, that.toActivationDate) && Objects.equals(membershipType, that.membershipType)
                && Objects.equals(minBalance, that.minBalance) && Objects.equals(maxBalance, that.maxBalance)
                && Objects.equals(monthsDuration, that.monthsDuration) && Objects.equals(productStatus, that.productStatus)
                && Objects.equals(totalPrice, that.totalPrice) && Objects.equals(currencyCode, that.currencyCode)
                && Objects.equals(sourceType, that.sourceType) && Objects.equals(memberAccountId, that.memberAccountId)
                && Objects.equals(memberAccountSearchRequest, that.memberAccountSearchRequest);
    }

    @Override
    public int hashCode() {
        return Objects.hash(website, status, autoRenewal, fromExpirationDate, toExpirationDate, fromCreationDate, toCreationDate,
                fromActivationDate, toActivationDate, membershipType, minBalance, maxBalance, monthsDuration, productStatus,
                totalPrice, currencyCode, sourceType, memberAccountId, withStatusActions, withMemberAccount, memberAccountSearchRequest);
    }
}
