package com.odigeo.membership.request.product;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.validation.constraints.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.ALWAYS)
public class UpdateMembershipRequest {

    @NotNull
    private String membershipId;
    @NotNull
    private String operation;
    private String membershipStatus;
    private String membershipRenewal;
    private Long userCreditCardId;

    public String getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(String membershipId) {
        this.membershipId = membershipId;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getMembershipStatus() {
        return membershipStatus;
    }

    public void setMembershipStatus(String membershipStatus) {
        this.membershipStatus = membershipStatus;
    }

    public String getMembershipRenewal() {
        return membershipRenewal;
    }

    public void setMembershipRenewal(String membershipRenewal) {
        this.membershipRenewal = membershipRenewal;
    }

    public Long getUserCreditCardId() {
        return userCreditCardId;
    }

    public UpdateMembershipRequest setUserCreditCardId(Long userCreditCardId) {
        this.userCreditCardId = userCreditCardId;
        return this;
    }
}
