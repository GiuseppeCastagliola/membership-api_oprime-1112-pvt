package com.odigeo.membership.request.search;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Objects;


@JsonIgnoreProperties(ignoreUnknown = true)
public class MemberAccountSearchRequest extends SearchRequest {

    private static final Logger LOGGER = LoggerFactory.getLogger(MemberAccountSearchRequest.class);
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private final String firstName;
    private final String lastName;
    private final Long userId;
    private final boolean withMemberships;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Long getUserId() {
        return userId;
    }

    public boolean isWithMemberships() {
        return withMemberships;
    }

    @JsonIgnore
    public MemberAccountSearchRequest(Builder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.userId = builder.userId;
        this.withMemberships = builder.withMemberships;
    }

    @JsonCreator
    private MemberAccountSearchRequest(@JsonProperty("firstName") String firstName, @JsonProperty("lastName") String lastName,
                                       @JsonProperty("userId") Long userId, @JsonProperty("withMemberships") boolean withMemberships) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userId = userId;
        this.withMemberships = withMemberships;
    }

    public static class Builder {

        private String firstName;
        private String lastName;
        private Long userId;
        private boolean withMemberships;

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder withMemberships(boolean withMemberships) {
            this.withMemberships = withMemberships;
            return this;
        }

        public MemberAccountSearchRequest build() {
            return new MemberAccountSearchRequest(this);
        }
    }

    public static MemberAccountSearchRequest valueOf(String jsonQueryParam) {
        try {
            return OBJECT_MAPPER.readValue(jsonQueryParam, MemberAccountSearchRequest.class);
        } catch (IOException e) {
            LOGGER.error("Invalid jsonQueryParameter : {}", jsonQueryParam, e);
            throw new MembershipInternalServerErrorException(e.getMessage(), e);
        }
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MemberAccountSearchRequest that = (MemberAccountSearchRequest) o;
        return withMemberships == that.withMemberships && Objects.equals(firstName, that.firstName)
                && Objects.equals(lastName, that.lastName) && Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, userId, withMemberships);
    }
}
