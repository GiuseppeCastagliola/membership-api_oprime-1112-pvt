package com.odigeo.membership.request.product.creation;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.annotate.JsonSubTypes.Type;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "typeInfo")
@JsonSubTypes({@Type(
    value = CreateNewMembershipRequest.class,
    name = "CreateNewMembershipRequest"
    ), @Type(
    value = CreatePendingToCollectRequest.class,
    name = "CreatePendingToCollectRequest"
    )})
public abstract class CreateMembershipRequest implements Serializable {

    private static final String WEBSITE_FIELD = "website";
    private static final String MONTHS_TO_RENEWAL = "monthsToRenewal";
    private static final String SOURCE_TYPE = "sourceType";
    private static final String MEMBERSHIP_TYPE = "membershipType";

    @NotNull
    private String website;
    @NotNull
    private int monthsToRenewal;
    @NotNull
    private String sourceType;
    @NotNull
    private String membershipType;

    CreateMembershipRequest() {
    }

    CreateMembershipRequest(String website, int monthsToRenewal, String sourceType, String membershipType) {
        this.website = website;
        this.monthsToRenewal = monthsToRenewal;
        this.sourceType = sourceType;
        this.membershipType = membershipType;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public int getMonthsToRenewal() {
        return monthsToRenewal;
    }

    public void setMonthsToRenewal(int monthsToRenewal) {
        this.monthsToRenewal = monthsToRenewal;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getMembershipType() {
        return membershipType;
    }

    public void setMembershipType(String membershipType) {
        this.membershipType = membershipType;
    }

    public Map<String, String> getAsMap() {
        Map<String, String> map = new HashMap<>();
        map.put(WEBSITE_FIELD, website);
        map.put(MONTHS_TO_RENEWAL, String.valueOf(monthsToRenewal));
        map.put(SOURCE_TYPE, sourceType);
        map.put(MEMBERSHIP_TYPE, membershipType);
        populateConcreteClassFields(map);
        return map;
    }

    protected abstract void populateConcreteClassFields(Map<String, String> map);
}
