package com.odigeo.membership.request.auth;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.http.auth.Credentials;

import java.security.Principal;

public class MembershipModuleCredentials implements Credentials {

    private final String name;
    private final String password;

    public MembershipModuleCredentials(String name, String password) {
        this.name = name;
        this.password = password;
    }

    @Override
    public Principal getUserPrincipal() {
        return new Principal() {
            @Override
            public String getName() {
                return name;
            }
        };
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("name", name)
                .append("password", password)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MembershipModuleCredentials)) {
            return false;
        }

        MembershipModuleCredentials that = (MembershipModuleCredentials) o;

        if (!name.equals(that.name)) {
            return false;
        }
        return getPassword().equals(that.getPassword());
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + getPassword().hashCode();
        return result;
    }
}
