package com.odigeo.membership.request.product;

import com.odigeo.membership.response.BlackListedPaymentMethod;

import java.util.List;

public class AddToBlackListRequest {

    private List<BlackListedPaymentMethod> blackListedPaymentMethods;

    public List<BlackListedPaymentMethod> getBlackListedPaymentMethods() {
        return blackListedPaymentMethods;
    }

    public void setBlackListedPaymentMethods(List<BlackListedPaymentMethod> blackListedPaymentMethods) {
        this.blackListedPaymentMethods = blackListedPaymentMethods;
    }
}
