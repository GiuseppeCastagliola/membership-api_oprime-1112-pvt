package com.odigeo.membership.mock;

import com.odigeo.membership.response.Money;
import com.odigeo.membership.response.PrimeBookingInformation;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class PrimeBookingInformationBuilderTest {

    private static final long BOOKING_ID = 1000;
    private static final String EUR = "EUR";
    private PrimeBookingInformationBuilder primeBookingInformationBuilder;

    @BeforeMethod
    public void setUp() throws Exception {
        primeBookingInformationBuilder = new PrimeBookingInformationBuilder(new Random());
    }

    @Test
    public void testBuildWithoutValues() throws IllegalAccessException {
        PrimeBookingInformation primeBookingInformation = primeBookingInformationBuilder.build();
        assertNotNull(primeBookingInformation.getDiscount());
        assertNotNull(primeBookingInformation.getPrice());
        assertNotNull(primeBookingInformation.getPrice().getCurrency());
        assertNotNull(primeBookingInformation.getPrice().getAmount());
    }

    @Test
    public void testBuildWithValues() throws IllegalAccessException {
        Money moneyOneEuro = new MoneyBuilder(new Random()).setAmount(BigDecimal.ONE).setCurrency(EUR).build();
        primeBookingInformationBuilder.setBookingId(BOOKING_ID);
        primeBookingInformationBuilder.setDiscount(moneyOneEuro);
        primeBookingInformationBuilder.setPrice(moneyOneEuro);
        PrimeBookingInformation primeBookingInformation = primeBookingInformationBuilder.build();
        assertEquals(primeBookingInformationBuilder.getBookingId(), primeBookingInformation.getBookingId());
        assertEquals(primeBookingInformationBuilder.getDiscount(), primeBookingInformation.getDiscount());
        assertEquals(primeBookingInformationBuilder.getPrice(), primeBookingInformation.getPrice());
    }
}
