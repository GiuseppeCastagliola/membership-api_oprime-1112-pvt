package com.odigeo.membership.util;

import com.odigeo.commons.test.random.Picker;

import java.util.Arrays;

public class PickerUtils {

    public static String pickWebsite(Picker picker) {
        return picker.pickFrom(Arrays.asList("ES", "GOFR", "OPDE"));
    }

    public static String pickCurrencyCode(Picker picker) {
        return picker.pickFrom(Arrays.asList("EUR", "GBP"));
    }

    public static String pickCreditCardName(Picker picker) {
        return picker.pickFrom(Arrays.asList("Visa Credit", "Mastercard Debit", "American Express"));
    }

    public static String pickMonth(Picker picker) {
        return picker.pickFrom(Arrays.asList("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"));
    }

    public static String pickStatus(Picker picker) {
        return picker.pickFrom(Arrays.asList("ENABLED", "DISABLED"));
    }

    public static String pickCreditCardType(Picker picker) {
        return picker.pickFrom(Arrays.asList("CREDITCARD", "PAYPAL", "SECURE3D", "BANKTRANSFER"));
    }

    public static String pickMembershipType(Picker picker) {
        return picker.pickFrom(Arrays.asList("BUSINESS", "BASIC"));
    }

    public static String pickSourceType(Picker picker) {
        return picker.pickFrom(Arrays.asList("FUNNEL_BOOKING", "POST_BOOKING"));
    }
}
