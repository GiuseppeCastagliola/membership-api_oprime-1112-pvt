package com.odigeo.membership.mock;

import com.odigeo.commons.test.random.Picker;
import com.odigeo.commons.test.random.StringPicker;
import com.odigeo.membership.response.CreditCard;
import com.odigeo.membership.response.CreditCardType;
import com.odigeo.membership.util.PickerUtils;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class CreditCardBuilder {

    private static final int CC_LENGTH = 16;
    private final Picker picker;
    private final StringPicker stringPicker;
    private Long id;
    private String creditCardNumber;
    private String expirationDateMonth;
    private String expirationDateYear;
    private String expirationDateYearFormated;
    private String owner;
    private CreditCardType creditCardType;


    public CreditCardBuilder(Random random) {
        picker = new Picker(random);
        stringPicker = new StringPicker(random);
    }

    public CreditCard build() {
        if (isNothingSet()) {
            return null;
        }
        fillMissingInfo();
        return buildCreditCard();
    }

    private CreditCard buildCreditCard() {
        CreditCard creditCard = new CreditCard();
        creditCard.setCreditCardNumber(creditCardNumber);
        creditCard.setCreditCardType(creditCardType);
        creditCard.setExpirationDateMonth(expirationDateMonth);
        creditCard.setExpirationDateYear(expirationDateYear);
        creditCard.setExpirationDateYearFormated(expirationDateYearFormated);
        creditCard.setId(id);
        creditCard.setOwner(owner);
        return creditCard;
    }

    private void fillMissingInfo() {
        if (id == null) {
            id = picker.pickLong(Long.MIN_VALUE, Long.MAX_VALUE);
        }
        if (creditCardNumber == null) {
            creditCardNumber = stringPicker.pickAsciiNumeric(CC_LENGTH);
        }
        if (expirationDateMonth == null) {
            expirationDateMonth = PickerUtils.pickMonth(picker);
        }
        if (expirationDateYear == null) {
            expirationDateYear = String.valueOf(picker.pickInt(16, 28));
        }
        if (expirationDateYearFormated == null) {
            expirationDateYearFormated = expirationDateMonth + expirationDateYear;
        }
        if (owner == null) {
            String name = stringPicker.pickAsciiAlphabetic(picker.pickInt(4, 10));
            String surname = stringPicker.pickAsciiAlphabetic(picker.pickInt(4, 10));
            owner = String.format("%s %s", name, surname);
        }
        if (creditCardType == null) {
            creditCardType = new CreditCardTypeBuilder().build();
        }

    }

    private boolean isNothingSet() {
        List<Serializable> fields = Arrays.asList(id, creditCardNumber, expirationDateMonth, expirationDateYear, expirationDateYearFormated, owner, creditCardType);
        for (Serializable field : fields) {
            if (field != null) {
                return false;
            }
        }
        return true;

    }

    public Long getId() {
        return id;
    }

    public CreditCardBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public CreditCardBuilder setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
        return this;
    }

    public String getExpirationDateMonth() {
        return expirationDateMonth;
    }

    public CreditCardBuilder setExpirationDateMonth(String expirationDateMonth) {
        this.expirationDateMonth = expirationDateMonth;
        return this;
    }

    public String getExpirationDateYear() {
        return expirationDateYear;
    }

    public CreditCardBuilder setExpirationDateYear(String expirationDateYear) {
        this.expirationDateYear = expirationDateYear;
        return this;
    }

    public String getExpirationDateYearFormated() {
        return expirationDateYearFormated;
    }

    public CreditCardBuilder setExpirationDateYearFormated(String expirationDateYearFormated) {
        this.expirationDateYearFormated = expirationDateYearFormated;
        return this;
    }

    public String getOwner() {
        return owner;
    }

    public CreditCardBuilder setOwner(String owner) {
        this.owner = owner;
        return this;
    }

    public CreditCardType getCreditCardType() {
        return creditCardType;
    }

    public CreditCardBuilder setCreditCardType(CreditCardType creditCardType) {
        this.creditCardType = creditCardType;
        return this;
    }
}
