package com.odigeo.membership.mock;

import com.odigeo.commons.test.random.Picker;
import com.odigeo.commons.test.random.StringPicker;
import com.odigeo.membership.response.CreditCardType;
import com.odigeo.membership.util.PickerUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class CreditCardTypeBuilder {
    private final StringPicker stringPicker;
    private final Picker picker;
    private String creditCardCode;
    private String creditCardName;
    private String creditCardType;

    public CreditCardTypeBuilder() {
        stringPicker = new StringPicker(new Random());
        picker = new Picker(new Random());
    }

    public CreditCardType build() {
        if (isNothingSet()) {
            return null;
        }
        fillMissingInfo();
        return buildCreditCardType();
    }

    private boolean isNothingSet() {
        List<String> fields = Arrays.asList(creditCardCode, creditCardName, creditCardType);
        for (String field : fields) {
            if (field != null) {
                return false;
            }
        }
        return true;
    }

    private CreditCardType buildCreditCardType() {
        CreditCardType ccType = new CreditCardType();
        ccType.setCreditCardCode(creditCardCode);
        ccType.setCreditCardName(creditCardName);
        ccType.setCreditCardType(creditCardType);
        return ccType;
    }

    private void fillMissingInfo() {
        if (creditCardCode == null) {
            stringPicker.pickAsciiAlphanumeric(2);
        }
        if (creditCardName == null) {
            PickerUtils.pickCreditCardName(picker);
        }
        if (creditCardType == null) {
            PickerUtils.pickCreditCardType(picker);
        }
    }

    public String getCreditCardCode() {
        return creditCardCode;
    }

    public CreditCardTypeBuilder setCreditCardCode(String creditCardCode) {
        this.creditCardCode = creditCardCode;
        return this;
    }

    public String getCreditCardName() {
        return creditCardName;
    }

    public CreditCardTypeBuilder setCreditCardName(String creditCardName) {
        this.creditCardName = creditCardName;
        return this;
    }

    public String getCreditCardType() {
        return creditCardType;
    }

    public CreditCardTypeBuilder setCreditCardType(String creditCardType) {
        this.creditCardType = creditCardType;
        return this;
    }
}
