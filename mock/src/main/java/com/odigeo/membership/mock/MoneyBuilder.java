package com.odigeo.membership.mock;

import com.odigeo.commons.test.random.Picker;
import com.odigeo.membership.response.Money;
import com.odigeo.membership.util.PickerUtils;

import java.math.BigDecimal;
import java.util.Random;

public class MoneyBuilder {

    private final Picker picker;
    private String currency;
    private BigDecimal amount;

    public MoneyBuilder(Random random) {
        picker = new Picker(random);
    }

    public Money build() {
        return fillMissingInfo().buildMoney();
    }

    private Money buildMoney() {
        Money money = new Money();
        money.setAmount(this.amount);
        money.setCurrency(this.currency);
        return money;
    }

    private MoneyBuilder fillMissingInfo() {
        if (amount == null) {
            setAmount(picker.pickDecimal(BigDecimal.ZERO, BigDecimal.valueOf(picker.pickLong(1L, 1000L))));
        }
        if (currency == null) {
            setCurrency(PickerUtils.pickCurrencyCode(picker));
        }
        return this;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public MoneyBuilder setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public String getCurrency() {
        return currency;
    }

    public MoneyBuilder setCurrency(String currency) {
        this.currency = currency;
        return this;
    }
}
