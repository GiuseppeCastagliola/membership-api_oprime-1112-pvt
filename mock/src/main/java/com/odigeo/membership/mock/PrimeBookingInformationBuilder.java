package com.odigeo.membership.mock;

import com.odigeo.commons.test.random.Picker;
import com.odigeo.membership.response.Money;
import com.odigeo.membership.response.PrimeBookingInformation;

import java.util.Random;

public class PrimeBookingInformationBuilder {

    private long bookingId;
    private Money price;
    private Money discount;

    private final Picker picker;

    public PrimeBookingInformationBuilder(Random random) {
        picker = new Picker(random);
    }

    public PrimeBookingInformation build() {
        return fillMissingInfo().buildPrimeBookingInformation();
    }

    private PrimeBookingInformation buildPrimeBookingInformation() {
        PrimeBookingInformation primeBookingInformation = new PrimeBookingInformation();
        primeBookingInformation.setBookingId(getBookingId());
        primeBookingInformation.setPrice(getPrice());
        primeBookingInformation.setDiscount(getDiscount());
        return primeBookingInformation;
    }

    private PrimeBookingInformationBuilder fillMissingInfo() {
        if (bookingId < 1) {
            setBookingId(picker.pickLong(1L, Long.MAX_VALUE));
        }
        if (price == null) {
            setPrice(new MoneyBuilder(new Random()).build());
        }
        if (discount == null) {
            setDiscount(new MoneyBuilder(new Random()).build());
        }
        return this;
    }

    public long getBookingId() {
        return bookingId;
    }

    public void setBookingId(long bookingId) {
        this.bookingId = bookingId;
    }

    public Money getPrice() {
        return price;
    }

    public void setPrice(Money price) {
        this.price = price;
    }

    public Money getDiscount() {
        return discount;
    }

    public void setDiscount(Money discount) {
        this.discount = discount;
    }
}
