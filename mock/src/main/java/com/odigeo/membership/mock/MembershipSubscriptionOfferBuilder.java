package com.odigeo.membership.mock;

import com.odigeo.commons.test.random.Picker;
import com.odigeo.commons.test.random.StringPicker;
import com.odigeo.membership.response.MembershipSubscriptionOffer;
import com.odigeo.membership.util.PickerUtils;

import java.math.BigDecimal;
import java.util.Random;

public class MembershipSubscriptionOfferBuilder {
    private Long productId;
    private String website;
    private String fareId;
    private BigDecimal price;
    private String currencyCode;

    private final Picker picker;
    private final StringPicker stringPicker;

    public MembershipSubscriptionOfferBuilder(Random random) {
        this.picker = new Picker(random);
        this.stringPicker = new StringPicker(random);
    }

    public MembershipSubscriptionOfferBuilder setProductId(Long productId) {
        this.productId = productId;
        return this;
    }

    public MembershipSubscriptionOfferBuilder setWebsite(String website) {
        this.website = website;
        return this;
    }

    public MembershipSubscriptionOfferBuilder setFareId(String fareId) {
        this.fareId = fareId;
        return this;
    }

    public MembershipSubscriptionOfferBuilder setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public MembershipSubscriptionOfferBuilder setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    public Long getProductId() {
        return productId;
    }

    public String getWebsite() {
        return website;
    }

    public String getFareId() {
        return fareId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public MembershipSubscriptionOffer build() {
        fillMissingInfo();
        return buildMembership();
    }

    private void fillMissingInfo() {
        if (this.fareId == null) {
            this.fareId = stringPicker.pickAsciiAlphabetic(picker.pickInt(4, 10));
        }
        if (this.price == null) {
            this.price = picker.pickPrice(100);
        }
        if (this.productId == null) {
            this.productId = Long.valueOf(1);
        }
        if (this.website == null) {
            this.website = stringPicker.pickAsciiAlphabetic(picker.pickInt(1, 2));
        }
        if (this.currencyCode == null) {
            this.currencyCode = PickerUtils.pickCurrencyCode(picker);
        }
    }

    @SuppressWarnings("PMD.NullAssignment")
    private MembershipSubscriptionOffer buildMembership() {
        MembershipSubscriptionOffer result = new MembershipSubscriptionOffer();

        result.setProductId(this.productId);
        result.setPrice(this.price);
        result.setWebsite(this.website);
        result.setOfferId(this.fareId);
        result.setCurrencyCode(this.currencyCode);

        return result;
    }

}
