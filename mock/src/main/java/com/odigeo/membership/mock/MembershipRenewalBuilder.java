package com.odigeo.membership.mock;

import com.odigeo.commons.test.random.Picker;
import com.odigeo.membership.response.MembershipRenewal;
import com.odigeo.membership.util.PickerUtils;
import org.apache.commons.lang.ObjectUtils;

import java.util.Random;

public class MembershipRenewalBuilder {

    private final Picker picker;
    private String renewalStatus;

    public MembershipRenewalBuilder(Random random) {
        picker = new Picker(random);
    }

    public MembershipRenewal build() {
        String renewal = String.valueOf(ObjectUtils.defaultIfNull(renewalStatus, PickerUtils.pickStatus(picker)));
        return new MembershipRenewal().setRenewalStatus(renewal);
    }

    public String getRenewalStatus() {
        return renewalStatus;
    }

    public void setRenewalStatus(String renewalStatus) {
        this.renewalStatus = renewalStatus;
    }
}
